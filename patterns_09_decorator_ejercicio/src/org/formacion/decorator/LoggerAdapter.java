package org.formacion.decorator;

import java.util.List;

public class LoggerAdapter implements BaseDatos {
	
	private BaseDatos baseDatos;
	private Logger logger;

	public LoggerAdapter(BaseDatos baseDatos, Logger logger) {
		this.baseDatos = baseDatos;
		this.logger = logger;
	}

	@Override
	public void inserta(String registro) {
		logger.add("inserta " + registro);
		baseDatos.inserta(registro);
	}

	@Override
	public List<String> registros() {
		logger.add("lectura");
		return baseDatos.registros();
	}

	

}
