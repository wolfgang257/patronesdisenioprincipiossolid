package org.formacion.facade;

public class RegistroFacade {

	public void registroExpres(String nombreCliente, String emailCliente) {
		// Codigo necesario para dar de alta un cliente con la configuracion tipica

		ServicioConfiguracion configuracion = new ServicioConfiguracion();
		ServicioFidelizacion fidelizacion = new ServicioFidelizacion();
		ServicioComunicacion comunicacion = new ServicioComunicacion();

		fidelizacion.crearTarjeta(nombreCliente, ServicioFidelizacion.Tipo.BASICA);
		configuracion.addEmail(nombreCliente, emailCliente);
		PreferenciasComunicacion preferencias = new PreferenciasComunicacion(false, true, true);
		comunicacion.setPreferencias(nombreCliente, preferencias);
	}
}
