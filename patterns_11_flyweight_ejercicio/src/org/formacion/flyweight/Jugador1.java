package org.formacion.flyweight;

public class Jugador1 {

	private Camiseta camiseta;

 	public Jugador1(Camiseta camiseta) {
		super();
		this.camiseta = camiseta;
	}

	public String dibuja() {
 		return camiseta.dibuja('1');
 	}

}

