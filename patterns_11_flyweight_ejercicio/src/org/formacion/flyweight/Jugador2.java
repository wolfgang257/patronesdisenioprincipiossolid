package org.formacion.flyweight;

public class Jugador2 {

	private Camiseta camiseta;

 	public Jugador2(Camiseta camiseta) {
		super();
		this.camiseta = camiseta;
	}

	public String dibuja() {
 		return camiseta.dibuja('2');
 	}

}
