package org.formacion.flyweight;

public class Jugador3 {

	private Camiseta camiseta;

 	public Jugador3(Camiseta camiseta) {
		super();
		this.camiseta = camiseta;
	}

	public String dibuja() {
 		return camiseta.dibuja('3');
 	}

}
