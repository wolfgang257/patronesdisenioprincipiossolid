package org.formacion.flyweight;

public class Main {

	public static void main(String[] args) {
		Camiseta camiseta = new Camiseta();
		
		System.out.println("\n\n------ jugador 1 -----\n\n");
		System.out.println(new Jugador1(camiseta).dibuja());
		System.out.println("\n\n------ jugador 2 -----\n\n");
		System.out.println(new Jugador2(camiseta).dibuja());
		System.out.println("\n\n------ jugador 3 -----\n\n");
		System.out.println(new Jugador3(camiseta).dibuja());
	}

}
