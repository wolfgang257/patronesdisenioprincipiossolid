package org.formacion.proxy;

public class CuentaPositiva implements Cuenta {
	
	private Cuenta componente;

	public CuentaPositiva(Cuenta componente) {
		super();
		this.componente = componente;
	}

	@Override
	public String getCliente() {
		return componente.getCliente();
	}

	@Override
	public int getCantidad() {
		return componente.getCantidad();
	}

	@Override
	public void movimiento(int importe) {
		if(getCantidad()+importe>=0) {
			componente.movimiento(importe);
		}else {
			//no pasa nada, más real si mandamos excepcion
			//throw new UnsupportedOperationException("No puede quedar en negativo");
		}
		
	}

}
