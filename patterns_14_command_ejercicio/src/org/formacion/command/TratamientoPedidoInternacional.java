package org.formacion.command;

public class TratamientoPedidoInternacional implements TratamientoPedido {

	private PedidoInternacional pedido;

	public TratamientoPedidoInternacional(PedidoInternacional pedido) {
		super();
		this.pedido = pedido;
	}

	@Override
	public boolean tratar() {
		return !pedido.getDestino().equals("Mordor");
	};
	
	
}
