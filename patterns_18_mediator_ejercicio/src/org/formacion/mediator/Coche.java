package org.formacion.mediator;

public class Coche {

	private Mediador mediador;
	
	public void setMediador(Mediador mediador) {
		this.mediador = mediador;
	}

	public void enciende() {
		mediador.enciendeRadio();
		mediador.apagaMusicaTelefono();
	}
	
	public void apaga() {
		mediador.apagaRadio();
	}
	
}
