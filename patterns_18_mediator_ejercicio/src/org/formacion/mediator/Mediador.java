package org.formacion.mediator;

public class Mediador {

	private Radio radio;
	private Telefono telefono;
	private Coche coche;
	
	
	public Mediador() {
		super();
	}


	public void setRadio(Radio radio) {
		this.radio = radio;
	}


	public void setTelefono(Telefono telefono) {
		this.telefono = telefono;
	}


	public void setCoche(Coche coche) {
		this.coche = coche;
	}


	public void apagaMusicaTelefono() {
		telefono.apagaMusica();
	}


	public void apagaRadio() {
		radio.apaga();
	}


	public void enciendeRadio() {
		radio.enciende();
	}
	
	
	
}
