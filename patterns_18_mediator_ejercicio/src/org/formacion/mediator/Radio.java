package org.formacion.mediator;

public class Radio {

	private boolean encendida = false;
	private Mediador mediador;
	
	public void setMediador(Mediador mediador) {
		this.mediador = mediador;
	}

	public void enciende() {
		encendida = true; 
		mediador.apagaMusicaTelefono();
	}
	
	public void apaga() {
		encendida = false; 
	}
	
	public boolean encendida() {
		return encendida;
	}
}
