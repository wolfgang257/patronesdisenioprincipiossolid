package org.formacion.mediator;

public class Telefono {

	private boolean musicaOn = false;
	private Mediador mediador;
	
	public void setMediador(Mediador mediador) {
		this.mediador = mediador;
	}

	public void recibeLlamada() {
		mediador.apagaRadio();
	}

	public void enciendeMusica() {
		musicaOn = true;
	}
	
	public void apagaMusica() {
		musicaOn = false;
	}
	
	public boolean musicaEncendida() {
		return musicaOn;
	}
}
