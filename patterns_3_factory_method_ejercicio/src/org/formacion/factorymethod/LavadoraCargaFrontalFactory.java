package org.formacion.factorymethod;

public class LavadoraCargaFrontalFactory extends LavadoraFactory{

	@Override
	protected Lavadora crearLavadora() {
		return new LavadoraCargaFrontal();
	}

}
