package org.formacion.factorymethod;

public class LavadoraCargaSuperiorFactory extends LavadoraFactory{

	@Override
	protected Lavadora crearLavadora() {
		return new LavadoraCargaSuperior();
	}

}
