package org.formacion.factorymethod;

public abstract class LavadoraFactory {

	public Lavadora crear() {
		Lavadora lavadora = crearLavadora();
		lavadora.ponerMandos();
		lavadora.ponerTambor();
		
		return lavadora;
	}

	protected abstract Lavadora crearLavadora();
}
