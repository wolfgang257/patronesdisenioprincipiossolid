package org.formacion.prototype;

public class Copia implements Cloneable{

	private int variable;
	
	public int getVariable() {
		return variable;
	}

	public void setVariable(int variable) {
		this.variable = variable;
	}

	@Override
	protected Copia clone(){
		try {
			return (Copia)super.clone();
		}catch(CloneNotSupportedException ex) {
			throw new AssertionError();
		}
	}

	
	
	
}
