package org.formacion.prototype;

import java.io.Serializable;

public class Unico implements Serializable{

	private static final long serialVersionUID = 9131691064011896238L;

	private static Unico INSTANCE;
	
	private float variable;
	
	private Unico() {}
	
	public synchronized static Unico getUnico() {
		if(INSTANCE==null) {
			INSTANCE = new Unico();
		}
		return INSTANCE;
	}
	
	@SuppressWarnings("unused")
	private Object readResolved(){
		return INSTANCE;
	}

	public float getVariable() {
		return variable;
	}

	public void setVariable(float variable) {
		this.variable = variable;
	}
}
