package org.formacion.ocp;

import java.util.ArrayList;
import java.util.List;

public interface GeneradorPrimos {

	
	public List<Integer> primos (int limit);
	
	public default boolean esPrimo (int candidato) {
		for (int i = 2; i < candidato; i++) {
			if (candidato % i == 0) {
				return false;
			}
		}
		
		return true;
	}
}
