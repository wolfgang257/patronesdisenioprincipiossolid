package org.formacion.ocp;

import java.util.ArrayList;
import java.util.List;

public class GeneradorPrimosAsc implements GeneradorPrimos{

	public List<Integer> primos (int limit) {
		
		List<Integer> primos = new ArrayList<>();
		for (int i = 2; i < limit; i++) {
			if (esPrimo(i)) {
				primos.add(i);
			}
		}
		return primos;

	}
}
