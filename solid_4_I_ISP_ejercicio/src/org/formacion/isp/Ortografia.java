package org.formacion.isp;

public interface Ortografia {

	public boolean correcto (Idioma idioma);
}
